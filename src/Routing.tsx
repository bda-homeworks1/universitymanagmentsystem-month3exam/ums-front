import { ReactElement, Suspense } from 'react'
import { BrowserRouter, Route, RouteObject, Router, Routes, useRoutes } from 'react-router-dom'

import { PAGES, PAGE_ADMINS, PAGE_GROUPS, PAGE_HOME, PAGE_STUDENTS, PAGE_TEACHERS } from '@/constants'
import Spinner from './components/lib/Spinner'
import Main from './components/layouts/Main'
import Home from './pages/Home'
import Admins from './pages/Admins'
import Teachers from './pages/Teachers'
import Students from './pages/Students'
import ProtectedRoute from './components/auth/ProtectedRoute'
import NotFound from './pages/NotFound'
import RoleProtectedRoute from './components/auth/RoleProtectedRoute'
import { Role } from './store/stores/AuthStore'
import Login from './pages/Login'
import Groups from './pages/Groups'

// const Elements = () => {
//   const elements = useRoutes(PAGES as Array<RouteObject>)
//   return elements
// }

export default function Routing(): ReactElement {
  return (
    <BrowserRouter>
    <Suspense fallback={<Spinner />}>
      <Routes>
        <Route path={'*'} element={<NotFound />} />
        <Route element={<ProtectedRoute />}>
          <Route path='/' element={<Main />}>
            <Route index element={<Home />} />
            <Route path={PAGE_HOME.path} element={<Home />} />
            <Route path={PAGE_ADMINS.path}>
              <Route index element={<Admins />} />
              <Route path={':id'} element={<Admins />} />
            </Route>
            <Route path={PAGE_TEACHERS.path}>
              <Route index element={<Teachers />} />
              <Route path={':id'} element={<Teachers />} />
            </Route>
            <Route path={PAGE_STUDENTS.path} element={<Students />} />
            {/* <Route path={PAGE_REPORTS.path} element={<Reports />} /> */}
            <Route element={<RoleProtectedRoute allowedRoles={[Role.ADMIN]}  />}>
              <Route path={PAGE_GROUPS.path} element={<Groups />} />
            </Route>
          </Route>
        </Route>
        <Route path='/login' element={<Login />} />
      </Routes>
    </Suspense>
  </BrowserRouter>
  )
}
