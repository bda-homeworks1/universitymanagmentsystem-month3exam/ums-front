import  {useMemo} from 'react';
import {RangePickerProps} from 'antd/es/date-picker/generatePicker'
import dayjs, { Dayjs } from 'dayjs'
import {PickerComponentClass} from 'antd/es/date-picker/generatePicker/interface';
// import {AntDatepicker} from '@/components/lib/DatePicker';

type NewType = RangePickerProps<Dayjs>

type DateRangeProps = Omit<PickerComponentClass<NewType, unknown>, 'onChange'> & {
	value?: [string, string]
	onChange?: (s: [string | null, string | null]) => void
}

// const {RangePicker} = AntDatepicker
const DateRange = (props: DateRangeProps) => {
	const {value,onChange,...rest} = props

	const handleOnChange = (dates:any, dateStrings: any) =>{
		if(onChange === undefined) return
		if(Boolean(dates)){
			onChange([dateStrings[0], dateStrings[1]])
		}
	}

	const val: [Dayjs, Dayjs] | undefined = useMemo(() => {
		if (value === undefined) return undefined
		const [date1, date2] = value
		return [dayjs(date1), dayjs(date2)]
	}, [value])
	return (
		// <RangePicker
		// 	value={val}
		// 	placeholder={['Başlanğıc', 'Son']}
		// 	onChange={handleOnChange}
		// 	{...rest}
		// />
		<div></div>
	);
};

export default DateRange;
