import { useStores } from '@/store'
import { Button, Dropdown, Menu } from 'antd'
import { useNavigate } from 'react-router-dom'
import Avatar from '@/components/lib/Avatar'
import { ACCESS_TOKEN_KEY, FULLNAME_LOCAL_STORAGE, ROLE_LOCAL_STORAGE } from '@/constants/storage'

interface IDropdownProps {
  src: string
  className: string
}

const DropdownMenu = ({ src, className }: IDropdownProps) => {
  const navigate = useNavigate()


  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: <Button onClick={() => { window.location.replace('/login')
            localStorage.removeItem(ACCESS_TOKEN_KEY)
            localStorage.removeItem(ROLE_LOCAL_STORAGE)
            localStorage.removeItem(FULLNAME_LOCAL_STORAGE)
          }
          }>Çıxış Et</Button>,
        },
      ]}
    />
  )
  return (
    <Dropdown overlay={menu} placement='bottom'>
      <Avatar size={'large'} className={className} src={src} />
    </Dropdown>
  )
}

export default DropdownMenu