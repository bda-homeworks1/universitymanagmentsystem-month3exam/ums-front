import { ReactElement } from 'react'
import style from './UserDropDown.module.scss'
import DropdownMenu from '@/components/lib/UserDropDown/DropdownMenu'
import { observer } from 'mobx-react'
import { FULLNAME_LOCAL_STORAGE, ROLE_LOCAL_STORAGE } from '@/constants'

function UserDropDown(): ReactElement {
  return (
    <div className={style.container}>
      <div className={style.details}>
        <h3 className={style.name}>{localStorage.getItem(FULLNAME_LOCAL_STORAGE)}</h3>
        <h6 className={style.name}>{localStorage.getItem(ROLE_LOCAL_STORAGE)}</h6>
      </div>
      <DropdownMenu
        className={style.dropDown}
        src={'https://gitlab.com/uploads/-/system/user/avatar/6419369/avatar.png?width=400'}
      />
    </div>
  )
}

export default observer(UserDropDown)