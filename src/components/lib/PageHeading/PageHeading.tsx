import { ReactNode } from 'react'
import style from './PageHeading.module.scss'

interface IPageHeadingProps {
  title: string
  children?: ReactNode
}

export default function PageHeading({ title, children }: IPageHeadingProps) {
  return (
    <div className={style.container}>
      <h2>{title}</h2>
      {children}
    </div>
  )
}