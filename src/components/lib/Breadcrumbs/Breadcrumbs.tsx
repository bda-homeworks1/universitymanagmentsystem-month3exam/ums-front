import { MenuItem } from '@/constants/types'
import { HomeOutlined, UserOutlined } from '@ant-design/icons'
import { Breadcrumb, Button } from 'antd'
import style from './Breadcrumbs.module.scss'

interface BreadcrumbsProperty {
  pageFirst?: MenuItem
  pageSecond?: MenuItem
  pageWithoutPath?: MenuItem
  children?: any
  addFunction?: any
  showButton?: boolean
}

const Breadcrumbs = ({
  pageFirst,
  pageSecond,
  pageWithoutPath,
  showButton = true,
  children,
  addFunction,
}: BreadcrumbsProperty) => (
  <div className={style.container}>
    <Breadcrumb separator={'>'} className={style.breadcrumb}>
      <Breadcrumb.Item href={'/'}>
        <HomeOutlined className={style.homeIcon} />
      </Breadcrumb.Item>
      {pageWithoutPath && (
        <Breadcrumb.Item>
          {pageWithoutPath.icon}
          <span style={{ marginLeft: '3px' }}>{pageWithoutPath.label}</span>
        </Breadcrumb.Item>
      )}
      {pageFirst && (
        <Breadcrumb.Item href={pageFirst.path} className={style.linkBread}>
          <span>{pageFirst.icon}</span>
          <span style={{ marginLeft: '3px' }}>{pageFirst.label}</span>
        </Breadcrumb.Item>
      )}

      {pageSecond && (
        <Breadcrumb.Item href={pageSecond.path} className={style.linkBread}>
          <span>{pageSecond.icon}</span>
          <span style={{ marginLeft: '3px' }}>{pageSecond.label}</span>
        </Breadcrumb.Item>
      )}

    </Breadcrumb>
    {showButton===true && (
          <div className={style.children}>
          <Button onClick={addFunction} style={{ margin: '10px' }} type='dashed' size={'small'}>
            Əlavə et
          </Button>
        </div>
      )}
  </div>
)

export default Breadcrumbs
