import { Form, Upload as AntUpload } from 'antd'
import { Rule } from 'antd/lib/form'
import { PHOTO_UPLOAD } from './constant'
import { ACCESS_TOKEN_KEY } from '@/constants'
import { useEffect, useState } from 'react'
import { UploadFile } from 'antd/lib/upload/interface'
import { UploadChangeParam } from 'antd/es/upload'
import style from './Upload.module.scss'
import ImgCrop from 'antd-img-crop'
import { FORM_CANCEL, FORM_CONFIRM, IMG_CROP_TITLE_TEXT } from '@/constants/forms'

const { Item } = Form
const { Dragger } = AntUpload

interface UploadProps {
  name: string
  label: string
  accept?: string
  maxCount?: number
  rules?: Rule[]
}

interface CustomDraggerProps {
  value?: string
  id?: string
  accept?: string
  maxCount?: number
  onChange?: (e: any) => void
}

const uploadUrl = import.meta.env.VITE_APP_BASE_URL
const imageUrl = import.meta.env.VITE_APP_BASE_URL_IMAGES

const CustomDragger = (props: CustomDraggerProps) => {
  const [fileList, setFileList] = useState<UploadFile[]>([])

  const onChange = (info: UploadChangeParam) => {
    console.log(info)

    if (info.file.status === 'done') {
      const { file } = info.file.response
      info.file.url = file
      props.onChange?.(info.file)
    }

    if (info.file.status === 'removed') {
      props.onChange?.(null)
    }

    // setFileList(info.fileList)
  }

  const onPreview = async (file: any) => {
    let src = file.url
    if (!Boolean(src)) {
      src = await new Promise((resolve, reject) => {
        const reader = new FileReader()
        if (file.originFileObj !== undefined) {
          reader.readAsDataURL(file.originFileObj)
        }
        reader.onprogress = (error) => reject(error)
        reader.onload = () => {
          if (reader.result !== null) {
            resolve(reader.result)
          }
        }
      })
    }
    const image = new Image()
    image.src = src
    window.open(src)
  }

  useEffect(() => {
    setFileList(
      props.value !== null && props.value !== undefined
        ? [
            {
              uid: `${Math.random() * 100}`,
              name: `${imageUrl}/${props.value}`,
              status: 'done',
              response: 'OK 200',
              url: `${imageUrl}/${props.value}`,
            },
          ]
        : []
    )
  }, [props.value])

  return (
    <ImgCrop
      aspect={327 / 469}
      modalTitle={IMG_CROP_TITLE_TEXT}
      modalCancel={FORM_CANCEL}
      modalOk={FORM_CONFIRM}
    >
      <Dragger
        action={`${uploadUrl}/upload`}
        headers={{ Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN_KEY)}` }}
        name='photo'
        className={style.dragger}
        id={props.id}
        fileList={fileList}
        accept={props.accept}
        onChange={onChange}
        onPreview={onPreview}
        maxCount={props.maxCount}
        listType={'picture-card'}
      >
        {PHOTO_UPLOAD}
      </Dragger>
    </ImgCrop>
  )
}

export default function Upload({ name, label, rules, accept, maxCount = 1 }: UploadProps) {
  return (
    <Item
      name={name}
      label={label}
      rules={rules}
      getValueFromEvent={(file) => {
        if (file === null) return null
        if (file.status === 'done') {
          return file.response.data
        }

        if (file.status === 'removed') {
          return null
        }
      }}
    >
      <CustomDragger accept={accept} maxCount={maxCount} />
    </Item>
  )
}
