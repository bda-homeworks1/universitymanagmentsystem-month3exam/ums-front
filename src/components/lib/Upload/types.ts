import { UploadFile } from 'antd'

export interface UploadChangeParam<T extends object = UploadFile> {
  file: T
  fileList: UploadFile[]
  event?: {
    percent: number
  }
}
