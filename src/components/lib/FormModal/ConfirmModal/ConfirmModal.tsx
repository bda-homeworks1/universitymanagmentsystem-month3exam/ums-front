import { Modal } from 'antd'

interface IProp {
  confirmModal: boolean
  toggle: (value: boolean) => void
  onOk: () => void
}

export default function ConfirmModal({ toggle, confirmModal, onOk }: IProp) {
  const handleOk = () => {
    toggle(false)
    onOk()
  }

  const handleCancel = () => {
    toggle(false)
  }

  return (
    <Modal
      title='Məlumatlar itəcək'
      open={confirmModal}
      onOk={handleOk}
      onCancel={handleCancel}
      okText={'Bəli'}
      cancelText={'Xeyr'}
    >
      <p>Bağlamaq istədiyinizdən əminsinizmi?</p>
    </Modal>
  )
}
