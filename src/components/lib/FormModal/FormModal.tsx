import {
  forwardRef,
  ForwardRefRenderFunction,
  ReactNode,
  useCallback,
  useImperativeHandle,
  useState,
} from 'react'
import { Form, Modal as AntModal } from 'antd'
import ConfirmModal from './ConfirmModal'
import { FORM_CANCEL, FORM_CONFIRM } from '@/constants/forms'

interface IFormModalProps {
  title?: string
  titleEdit?: string
  children?: (isEditing: boolean) => ReactNode
  onSubmit: (data: unknown) => Promise<void>
  onEdit?: (id: number, data: unknown) => Promise<void>
}

export interface IFormModalHandle {
  open: () => void
  close: () => void
  setEdit: (data: object) => void
  isEditing: () => boolean
}

const FormModal: ForwardRefRenderFunction<IFormModalHandle, IFormModalProps> = (
  { title, titleEdit, onSubmit, children, onEdit },
  ref
) => {
  const { useForm } = Form
  const [form] = useForm()
  const [isLoading, setIsLoading] = useState<boolean>()
  const [isVisible, setIsVisible] = useState<boolean>()
  const [isEditing, setIsEditing] = useState<boolean>(false)

  const [confirmModal, setConfirmModal] = useState<boolean>(false)

  const closeModal = useCallback((): void => {
    if (form.isFieldsTouched()) {
      return setConfirmModal(true)
    }
    return setIsVisible(false)
  }, [form])

  const onFinalCancel = useCallback((): void => {
    form.resetFields()
    setIsVisible(false)
  }, [form])

  useImperativeHandle(ref, () => ({
    open: () => {
      setIsVisible(true)
      setIsEditing(false)
      form.resetFields()
    },
    close: () => {
      closeModal()
    },
    setEdit: (data: object) => {
      setIsVisible(true)
      setIsEditing(true)
      form.setFieldsValue(data)
    },
    isEditing: () => {
      return isEditing
    },
  }))

  const onFormSubmit = useCallback(() => {
    const data = form.getFieldsValue()

    setIsLoading(true)

    const promiseToBe =
      isEditing && onEdit !== undefined ? onEdit(form.getFieldValue('id'), data) : onSubmit(data)

    promiseToBe
      .then(() => {
        form.resetFields()
        setConfirmModal(false)
        setIsVisible(false)
      })
      .catch((e) => {
        if (e.errors !== undefined) {
          const ktf = e.errors as Record<string, string[]>

          const res = Object.entries(ktf)
            .filter(([, value]) => value !== undefined)
            .map(([key, value]) => ({
              name: key,
              errors: value,
            }))

          form.setFields(res)
          form.scrollToField(res[0]?.name)
        }
      })
      .finally(() => {
        setIsLoading(false)
      })
  }, [form, isEditing, onEdit, onSubmit])

  return (
    <>
      <ConfirmModal toggle={setConfirmModal} onOk={onFinalCancel} confirmModal={confirmModal} />
      <AntModal
        width={window.screen.width * 0.75}
        okText={FORM_CONFIRM}
        cancelText={FORM_CANCEL}
        title={isEditing ? titleEdit : title}
        open={isVisible}
        onOk={form.submit}
        onCancel={closeModal}
        destroyOnClose={confirmModal}
        mask={false}
        maskClosable={false}
      >
        <Form disabled={isLoading} onFinish={onFormSubmit} layout='vertical' form={form}>
          {children?.(isEditing)}
        </Form>
      </AntModal>
    </>
  )
}

export default forwardRef(FormModal)
