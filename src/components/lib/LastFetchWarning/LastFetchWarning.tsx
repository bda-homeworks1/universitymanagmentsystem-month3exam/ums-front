import { Alert, Button } from 'antd'
import { useEffect, useMemo, useState } from 'react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import classNames from 'classnames'
import style from './LastFetchWarning.module.scss'

import 'dayjs/locale/az'

dayjs.extend(relativeTime)
dayjs.locale('az')

interface LastFetchWarningProps {
  lastFetched: Date
  update: () => void
  loading?: boolean
  className?: string
}

const getText = (t: string) => `Məlumatlar ${t} yenilənib`
const getLoadingText = (t: number) => `Məlumatlar yenilənir${'.'.repeat(t)}`

export default function LastFetchWarning({
  lastFetched,
  update,
  className,
  loading,
}: LastFetchWarningProps) {
  const [text, setText] = useState<string>()
  const [loadingTextCount, setLoadingTextCount] = useState<number>(0)

  useEffect(() => {
    setText(getText(dayjs(lastFetched).fromNow()))
    const interval = setInterval(() => {
      setText(getText(dayjs(lastFetched).fromNow()))
    }, 1000)

    return () => clearInterval(interval)
  }, [lastFetched])

  useEffect(() => {
    let interval: NodeJS.Timer

    if (loading === true) {
      interval = setInterval(() => {
        setLoadingTextCount((prev) => (prev + 1) % 4)
      }, 100)
    }

    return () => {
      if (interval !== undefined) {
        clearInterval(interval)
      }
    }
  }, [loading])

  const finalLoading = useMemo(
    () => (loading === true ? getLoadingText(loadingTextCount) : text),
    [loading, loadingTextCount, text]
  )

  return (
    <Alert
      className={classNames(style.container, className)}
      message={finalLoading}
      type='info'
      action={
        <Button loading={loading} onClick={update} size='small'>
          Yenilə
        </Button>
      }
      showIcon
    />
  )
}