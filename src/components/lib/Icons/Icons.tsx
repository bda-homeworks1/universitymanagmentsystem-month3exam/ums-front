import { IconsType, IconsTypeMapping } from "./types";
import { useMemo, Suspense } from "react";
import { AntdIconProps } from "@ant-design/icons/lib/components/AntdIcon";

type IconsComponentProps = AntdIconProps & {
    icon: IconsType
}

const IconsComponent = ({icon, ...rest}: IconsComponentProps) => {
    const Component = useMemo(() => IconsTypeMapping[icon], [icon])

    return(
        <Suspense fallback={<></>}>
            <Component {...rest}/>
        </Suspense>
    )
}

export default IconsComponent