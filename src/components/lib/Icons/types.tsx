import { lazy, LazyExoticComponent } from 'react'

const HomeOutlined = lazy(async () => import('@ant-design/icons/HomeOutlined'))
const Teachers = lazy(async () => import('@ant-design/icons/SolutionOutlined'))
{/* <SolutionOutlined /> */}

export enum IconsType {
    HomeOutlined,
    Teachers
}

export const IconsTypeMapping: Record<IconsType, LazyExoticComponent<any>> = {
    [IconsType.HomeOutlined] : HomeOutlined,
    [IconsType.Teachers] : Teachers
}