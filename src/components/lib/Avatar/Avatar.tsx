import { Avatar as AntAvatar, AvatarProps as AntAvatarProps } from 'antd'

interface AvatarProps extends AntAvatarProps {}

export default function Avatar({ src, ...rest }: AvatarProps) {
  return <AntAvatar {...rest} src={src} />
}