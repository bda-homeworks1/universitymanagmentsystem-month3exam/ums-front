import { PLACE_HOLDER_ENDING, REQUIRED_FIELD_EMAIL, REQUIRED_FIELD_EMAIL_RULES, REQUIRED_FIELD_PASSWORD_RULES, REQUIRED_FIELD_RULES } from "@/constants/forms";
import { useStores } from "@/store";
import { ITeacher } from "@/store/stores/TeachersStore";
import { Button, Col, Form, Input, Modal, Row } from "antd";
import { useState } from "react";
import styles from './TeachersForm.module.scss'
import { useNavigate } from "react-router-dom";
import { PAGE_TEACHERS } from "@/constants";

const {Item} = Form

interface TeacherModalProps {
    isEditing: boolean
}

export default function TeacherForm({isEditing} : TeacherModalProps) {
    const {teachersStore} = useStores()
    const [teacher, setTeacher] = useState<Partial<ITeacher>>({
        name: '',
        surname: '',
        username: '',
        password: '',
        email: '',
        subject: '',
        roleId: 2
    })
    const navigate = useNavigate()


    const createTeacher = (data: Partial<ITeacher>) => {
        teachersStore.createTeacher(data)
        navigate(`${PAGE_TEACHERS.path}`)
        teachersStore.getTeachers()
    
    }

    return(
        <div>
            <Row>
                <Col span={24}>
                    <Item name={'name'} label='Ad' rules={REQUIRED_FIELD_RULES}>
                        <Input name='name' 
                        onChange={(e) => setTeacher({...teacher, name: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <Item name={'surname'} label='Soyad' rules={REQUIRED_FIELD_RULES}>
                        <Input name='surname' 
                        onChange={(e) => setTeacher({...teacher, surname: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <Item name={'username'} label='Username' rules={REQUIRED_FIELD_RULES}>
                        <Input name='username' 
                        onChange={(e) => setTeacher({...teacher, username: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <Item name={'password'} label='Password' rules={REQUIRED_FIELD_PASSWORD_RULES}>
                        <Input.Password name='password' 
                        onChange={(e) => setTeacher({...teacher, password: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <Item name={'email'} label='Email' rules={REQUIRED_FIELD_EMAIL_RULES}>
                        <Input name='email' 
                        onChange={(e) => setTeacher({...teacher, email: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <Item name={'subject'} label='Fənn' rules={REQUIRED_FIELD_RULES}>
                        <Input name='subject' 
                        onChange={(e) => setTeacher({...teacher, subject: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item>
                    <div className={styles.buttons}>
                        <Button className={styles.submitButton} type="primary" onClick={() => createTeacher(teacher)}>
                            Əlavə et
                        </Button>
                    </div>
                    {/* <Item name={'email'} label='Email' rules={REQUIRED_FIELD_EMAIL_RULES}>
                        <Input name='email' 
                        onChange={(e) => setTeacher({...teacher, name: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item> */}
                </Col>
            </Row>
        </div>
    )

}