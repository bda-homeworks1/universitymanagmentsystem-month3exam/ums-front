import { Form, Input, InputNumber, Select, Col, Row, Button } from 'antd'
import DatePicker from '@/components/lib/DatePicker'
import {
  FORM_NUMBER_INPUT_MASK,
  GENDERS,
  REQUIRED_FIELD_RULES,
  REQUIRED_FIELD_FILE,
  REQUIRED_NUMBER_FIELD_RULES,
  STATUSESCUSTOMERS,
  REQUIRED_FIELD_EMAIL_RULES,
  PLACE_HOLDER_ENDING,
  REQUIRED_INPUT_MASK,
  EMAIL_ADDRESS_PLACEHOLDER,
} from '@/constants/forms'
// import { MaskedInput } from 'antd-mask-input'
import Upload from '@/components/lib/Upload'
import styles from './GroupsForm.module.scss'
import { useState } from 'react'
import { useStores } from '@/store'
import { IGroup } from '@/store/stores/GroupsStore'

const { Item } = Form

interface GroupModalProps {
  isEditing: boolean
}

export default function GroupsForm({ isEditing }: GroupModalProps) {
  const { groupsStore } = useStores()
  const [group, setGroup] = useState<Partial<IGroup>>({
    name: '',
  })

  const createGroup = (data: Partial<IGroup>) => {
    groupsStore.createGroup(data)
    console.log({ ...data })
  }

  return (
    <Form>
      <Row>
        <Col className={styles.colLeft} span={24}>
          <Item name={'name'} label={'Qrupun adi'} rules={REQUIRED_FIELD_RULES}>
            <Input
              name={'name'}
              onChange={(e) => setGroup({ ...group, name: e.target.value })}
              className={styles.item}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>

          {/* <Item name={'birthdate'} label={'Doğulduğu Tarix'} rules={REQUIRED_NUMBER_FIELD_RULES}>
            <DatePicker />
          </Item> */}
        </Col>
      </Row>
      <div className={styles.buttons}>
        <Button className={styles.submitButton} type='primary' onClick={() => createGroup({ ...group })}>
          Əlavə et
        </Button>
      </div>
      {/* {isEditing && (
        <Item name={'status'} label={'Status'} rules={REQUIRED_NUMBER_FIELD_RULES}>
          <Select options={STATUSESCUSTOMERS} />
        </Item>
      )} */}

      {/* <Upload rules={REQUIRED_FIELD_FILE} name={'imageUrl'} label={'Şəkil'} /> */}
    </Form>
  )
}
