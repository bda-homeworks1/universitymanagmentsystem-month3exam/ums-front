import {
  PLACE_HOLDER_ENDING,
  REQUIRED_FIELD_EMAIL,
  REQUIRED_FIELD_EMAIL_RULES,
  REQUIRED_FIELD_PASSWORD_RULES,
  REQUIRED_FIELD_RULES,
} from '@/constants/forms'
import { useStores } from '@/store'
import { Button, Col, Form, Input, Modal, Row, Select } from 'antd'
import { useEffect, useState } from 'react'
import styles from './StudentsForm.module.scss'
import { useNavigate } from 'react-router-dom'
import { PAGE_STUDENTS } from '@/constants'
import { IStudent } from '@/store/stores/StudentsStore'
import { IGroup } from '@/store/stores/GroupsStore'

const { Item } = Form

interface StudentModalProps {
  isEditing: boolean
}
interface SelectProps {
  value: any,
  label: any
}

export default function StudentForm({ isEditing }: StudentModalProps) {
  const { studentsStore, groupsStore } = useStores()
  const [student, setStudent] = useState<Partial<IStudent>>({
    name: '',
    surname: '',
    username: '',
    password: '',
    email: '',
    groupId: 0,
    roleId: 3,
  })
  const navigate = useNavigate()
  
  let optionsArr: SelectProps[] = []
  

  groupsStore?.groups?.map((item: IGroup) => {
    optionsArr.push({value: item.id, label: item.name})
  })


  useEffect(() => {
    groupsStore.getGroups()
    console.log(optionsArr, 'options')
  }, [groupsStore])


  

  const createTeacher = async(data: Partial<IStudent>) => {
    studentsStore.createStudent(data)
    navigate(`${PAGE_STUDENTS.path}`)
    studentsStore.getStudents()
  }

  return (
    <div>
      <Row>
        <Col span={18}>
          <Item name={'name'} label='Ad' rules={REQUIRED_FIELD_RULES}>
            <Input
              name='name'
              onChange={(e) => setStudent({ ...student, name: e.target.value })}
              className={styles.input}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'surname'} label='Soyad' rules={REQUIRED_FIELD_RULES}>
            <Input
              name='surname'
              onChange={(e) => setStudent({ ...student, surname: e.target.value })}
              className={styles.input}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'username'} label='Username' rules={REQUIRED_FIELD_RULES}>
            <Input
              name='username'
              onChange={(e) => setStudent({ ...student, username: e.target.value })}
              className={styles.input}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'password'} label='Password' rules={REQUIRED_FIELD_PASSWORD_RULES}>
            <Input.Password
              name='password'
              onChange={(e) => setStudent({ ...student, password: e.target.value })}
              className={styles.input}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
        </Col>
        <Col span={18}>
         
          <Item name={'email'} label='Email' rules={REQUIRED_FIELD_EMAIL_RULES}>
            <Input
              name='email'
              onChange={(e) => setStudent({ ...student, email: e.target.value })}
              className={styles.input}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'groupId'} label='Qrup' rules={REQUIRED_FIELD_EMAIL_RULES}>
          <Select
            showSearch
            style={{ width: 200 }}
            onChange={(e) => setStudent({...student, groupId: e})}
            placeholder='Search to Select'
            optionFilterProp='children'
            filterOption={(input, option) => (option?.label ?? '').includes(input)}
            filterSort={(optionA, optionB) =>
              (optionA?.label ?? '')
                .toLowerCase()
                .localeCompare((optionB?.label ?? '').toLowerCase())
            }
            options={optionsArr}
          />
          </Item>

   
          {/* <Item name={'email'} label='Email' rules={REQUIRED_FIELD_EMAIL_RULES}>
                        <Input name='email' 
                        onChange={(e) => setTeacher({...teacher, name: e.target.value})}
                        className={styles.input}
                        type={'text'}
                        placeholder={`Ad ${PLACE_HOLDER_ENDING}`}/>
                    </Item> */}
        </Col>
      </Row>
      <div className={styles.buttons}>
            <Button
              className={styles.submitButton}
              type='primary'
              onClick={() => createTeacher(student)}
            >
              Əlavə et
            </Button>
          </div>
    </div>
  )
}
