import { Form, Input, InputNumber, Select, Col, Row, Button } from 'antd'
import DatePicker from '@/components/lib/DatePicker'
import {
  FORM_NUMBER_INPUT_MASK,
  GENDERS,
  REQUIRED_FIELD_RULES,
  REQUIRED_FIELD_FILE,
  REQUIRED_NUMBER_FIELD_RULES,
  STATUSESCUSTOMERS,
  REQUIRED_FIELD_EMAIL_RULES,
  PLACE_HOLDER_ENDING,
  REQUIRED_INPUT_MASK,
  EMAIL_ADDRESS_PLACEHOLDER,
} from '@/constants/forms'
// import { MaskedInput } from 'antd-mask-input'
import Upload from '@/components/lib/Upload'
import styles from './AdminsForm.module.scss'
import { useState } from 'react'
import { IAdmin } from '@/store/stores/AdminsStore'
import { useStores } from '@/store'

const { Item } = Form

interface AdminsModalProps {
  isEditing: boolean
}

export default function AdminsForm({ isEditing }: AdminsModalProps) {
  const { adminsStore } = useStores()
  const [admin, setAdmin] = useState<Partial<IAdmin>>({
    name: '',
    surname: '',
    username: '',
    password: '',
    email: '',
    roleId: 1,
  })

  const createAdmin = (data: Partial<IAdmin>) => {
    adminsStore.createAdmin(data)
    console.log({ ...data })
  }

  return (
    <div>
      <Row>
        <Col className={styles.colLeft} span={24}>
          <Item name={'name'} label={'Ad'} rules={REQUIRED_FIELD_RULES}>
            <Input
              name={'name'}
              onChange={(e) => setAdmin({ ...admin, name: e.target.value })}
              className={styles.item}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'surname'} label={'Soyad'} rules={REQUIRED_FIELD_RULES}>
            <Input
              name={'surname'}
              onChange={(e) => setAdmin({ ...admin, surname: e.target.value })}
              className={styles.item}
              type={'text'}
              placeholder={`Soyad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'username'} label={'Username'} rules={REQUIRED_FIELD_RULES}>
            <Input
              name={'username'}
              onChange={(e) => setAdmin({ ...admin, username: e.target.value })}
              className={styles.item}
              type={'text'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'password'} label={'Password'} rules={REQUIRED_FIELD_RULES}>
            <Input.Password
              name={'password'}
              onChange={(e) => setAdmin({ ...admin, password: e.target.value })}
              className={styles.item}
              type={'password'}
              placeholder={`Ad ${PLACE_HOLDER_ENDING}`}
            />
          </Item>
          <Item name={'email'} label={'Elektron poçt ünvanı'} rules={REQUIRED_FIELD_EMAIL_RULES}>
            <Input
              name={'email'}
              onChange={(e) => setAdmin({ ...admin, email: e.target.value })}
              className={styles.item}
              type='email'
              placeholder={`${EMAIL_ADDRESS_PLACEHOLDER}`}
            />
          </Item>
          {/* <Item name={'birthdate'} label={'Doğulduğu Tarix'} rules={REQUIRED_NUMBER_FIELD_RULES}>
            <DatePicker />
          </Item> */}
        </Col>
      </Row>
      <div className={styles.buttons}>
        <Button className={styles.submitButton} type='primary' onClick={() => createAdmin({ ...admin })}>
          Əlavə et
        </Button>
      </div>
      {/* {isEditing && (
        <Item name={'status'} label={'Status'} rules={REQUIRED_NUMBER_FIELD_RULES}>
          <Select options={STATUSESCUSTOMERS} />
        </Item>
      )} */}

      {/* <Upload rules={REQUIRED_FIELD_FILE} name={'imageUrl'} label={'Şəkil'} /> */}
    </div>
  )
}
