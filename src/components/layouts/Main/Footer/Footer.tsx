import { ReactElement } from 'react'
import { Layout } from 'antd'
import style from './Footer.module.scss'
import _package from '@/../package.json'

export default function Footer(): ReactElement {
  return (
    <Layout.Footer className={style.footer}>
      <p className={style.copyRight}>
        © «Caspian Entertainment» - {new Date().getFullYear()} Version {_package.version}
      </p>
    </Layout.Footer>
  )
}