import style from './Header.module.scss'
import { Layout } from 'antd'
import { ReactElement } from 'react'
import classNames from 'classnames'
import UserDropDown from '@/components/lib/UserDropDown'
import _package from '@/../package.json'

export interface IHeaderProps {
  isSideBarOpen: boolean
}

export default function Header({ isSideBarOpen }: IHeaderProps): ReactElement {
  return (
    <Layout.Header className={style.container}>
      <div className={style.header}>
        <h1
          className={classNames(style.title, {
            [style.title_open]: isSideBarOpen,
          })}
        >
         University Management System
        </h1>
      </div>
      <UserDropDown />
    </Layout.Header>
  )
}