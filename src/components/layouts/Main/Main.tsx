import { ReactElement, Suspense, useState } from 'react'
import { Layout } from 'antd'
import { Outlet } from 'react-router-dom'
import Header from './Header'
import SideBar from './SideBar'
import style from './Main.module.scss'
import Footer from './Footer'
import Spinner from '@/components/lib/Spinner'
const { Content } = Layout

export default function Main(): ReactElement {
  const [isSideBarOpen, setIsSideBarOpen] = useState<boolean>(true)

  return (
    <Layout>
      <Header isSideBarOpen={isSideBarOpen} />
      <Layout>
        <SideBar toggle={() => setIsSideBarOpen((prev) => !prev)} isOpen={isSideBarOpen} />
        <Content>
          <div className={style.content}>
            <Suspense fallback={Spinner}>
            <Outlet />
            </Suspense>
          </div>
          <Footer />
        </Content>
      </Layout>
    </Layout>
  )
}