import { PAGES } from "@/constants/navigation";
import { Role } from "@/store/stores/AuthStore";
import { Link } from "react-router-dom";

export const getMenuItems = (role: Role) =>
  PAGES[role].map((page) => ({
    ...page,
    label: page.children !== undefined ? page.label : <Link to={page.path}>{page.label}</Link>,
    key: page.path,
    children: page.children?.map((child) => ({
      ...child,
      key: child.path,
      label: <Link to={child.path}>{child.label}</Link>,
    })),
  }))