import { ReactElement, useMemo } from 'react'
import { Menu, Layout } from 'antd'
import { useLocation } from 'react-router-dom'
import { getMenuItems } from './utils'
import style from './SideBar.module.scss'
import { observer } from 'mobx-react'
import img from '@/assets/logo_transparent.png'
import closeImg from '@/assets/close_logo_transparent.png'
import { useStores } from '@/store'
import { Role, RoleMappings } from '@/store/stores/AuthStore'
import { ROLE_LOCAL_STORAGE } from '@/constants'

const { Sider } = Layout

interface ISideBarProps {
  isOpen: boolean
  toggle: () => void
}

function SideBar({ isOpen, toggle }: ISideBarProps): ReactElement {
  const location = useLocation()
  const { authStore } = useStores()


  const items = useMemo(
    () => getMenuItems(RoleMappings[0]===localStorage.getItem(ROLE_LOCAL_STORAGE)?Role.ADMIN:RoleMappings[1]===localStorage.getItem(ROLE_LOCAL_STORAGE)?Role.TEACHER:Role.STUDENT),
    [RoleMappings[0]===localStorage.getItem(ROLE_LOCAL_STORAGE)?Role.ADMIN:RoleMappings[1]===localStorage.getItem(ROLE_LOCAL_STORAGE)?Role.TEACHER:Role.STUDENT]
  )

  return (
    <Sider className={style.menu} collapsible collapsed={!isOpen} onCollapse={() => toggle()}>
      <div className={style.logo}>
        {isOpen && <img className={style.img} src={img} alt='img' />}

        {!isOpen && <img className={style.closeImg} src={closeImg} alt='img' />}
      </div>

      <Menu
        className={style.menutable}
        theme='light'
        selectedKeys={[location.pathname]}
        mode='inline'
        items={items}
      />
    </Sider>
  )
}

export default observer(SideBar)