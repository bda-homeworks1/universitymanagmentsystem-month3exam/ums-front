import { observer } from 'mobx-react'
import { Navigate, Outlet } from 'react-router-dom'
import Spinner from '@/components/lib/Spinner'
import { PAGES, PAGE_HOME, PAGE_LOGIN } from '@/constants/navigation'
import { useStores } from '@/store'

const ProtectedRoute = () => {
  const { authStore } = useStores()

  if (authStore.isLoading) {
    return <Spinner />
  }

  if (!authStore.isAuthenticated) {
    console.log('Navigate Login')
    return <Navigate to={PAGE_LOGIN.path} replace />
  } else {
    console.log('Outlet')
    return <Outlet />
  }

}

export default observer(ProtectedRoute)
