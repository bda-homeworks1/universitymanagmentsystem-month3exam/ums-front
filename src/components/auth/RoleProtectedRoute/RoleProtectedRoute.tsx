import { observer } from 'mobx-react'
import { Navigate, Outlet } from 'react-router-dom'
import { PAGE_HOME, PAGE_LOGIN } from '@/constants/navigation'
import { useStores } from '@/store'
import { Role, RoleMappings } from '@/store/stores/AuthStore'
import { ROLE_LOCAL_STORAGE } from '@/constants'

interface RoleProtectedRouteProps {
  allowedRoles: Role[]
}

const RoleProtectedRoute = ({ allowedRoles }: RoleProtectedRouteProps) => {
  const { authStore } = useStores()
  const adminRole: string = allowedRoles[0] + ''
  const teacherRole: string = allowedRoles[1] + ''
  const studentRole: string = allowedRoles[2] + ''


  if(localStorage.getItem(ROLE_LOCAL_STORAGE) !== null){
    if ( localStorage.getItem(ROLE_LOCAL_STORAGE)?.includes(RoleMappings[Role.ADMIN]) || localStorage.getItem(ROLE_LOCAL_STORAGE)?.includes(RoleMappings[Role.TEACHER]) || localStorage.getItem(ROLE_LOCAL_STORAGE)?.includes(RoleMappings[Role.STUDENT]) ) {
      console.log('allowed RoleProtected Route')
      return <Outlet />
    }
  }
  
  console.log(adminRole, 'adminRole')
  console.log(teacherRole, 'teacherRole')
  console.log(studentRole, 'studedntRole')

  console.log('Navigate Home')
  return <Navigate to={PAGE_HOME.path} replace />
}

export default observer(RoleProtectedRoute)
