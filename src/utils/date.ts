import dayjs from 'dayjs'
import {DATE_FORMAT, TIME_FORMAT} from '../constants'


export const formatNullValues = (datum?: unknown) => {
    if (datum !== undefined) return datum
    return 'Məlumat Yoxdur'
}

export const formatDate = (date?: Date | null): string => {
    return dayjs(date).format(DATE_FORMAT)
}

export const formatDateAndTime = (date: Date): string => {
    return dayjs(date).format(`${DATE_FORMAT} ${TIME_FORMAT}`)
}

export const fullNameParser = (data: any) => {
    if (data.fullName === undefined) {
        return data
    }
    const s = data.fullName.trim().split(' ')
    if (s.length === 1) {
        return ({
            ...data,
            firstName: s[0],
            lastName: s[0],
        })
    }

    if (s.length > 1) {
        return ({
            ...data,
            firstName: s[0],
            lastName: s[1],
        })
    }

    return data
}