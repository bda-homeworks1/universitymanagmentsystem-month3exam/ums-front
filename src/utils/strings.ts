export const capitalize = (s: string) => {
    return s
        .split(/\s+/g)
        .map((f) => f[0].toUpperCase() + f.slice(1, f.length))
        .join(' ')
}