export type FullNameAdder<T> = T & {
    fullName: string
  }