export const downloadExcelFile = (res: Blob) => {
    window.location.href = URL.createObjectURL(
        new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
    )
}