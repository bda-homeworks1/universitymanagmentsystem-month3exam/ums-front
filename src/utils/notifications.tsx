import { message, notification } from 'antd'
import { AxiosError } from 'axios'
import React from 'react'
import { capitalize } from './strings'

export const openErrorNotification = (errorEntity: string, e: AxiosError) => {
  notification.error({
    message: <div>{errorEntity} - Məlumat mübadiləsində xəta baş verdi</div>,
    placement: 'bottomRight',
    description: e.message,
  })
}

export const showSuccessMessage = () => {
  void message.success('Əməliyyat uğurla tamamlandı').then()
}

export const showErrorMessage = () => {
  void message.error('Əməliyyat uğursuz oldu').then()
}

export const showWelcomeMessage = (name: string) => {
  void message.success(`${capitalize(name)} xoş gəldiniz.`).then()
}