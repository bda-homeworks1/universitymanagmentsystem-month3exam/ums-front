export const LAST_FETCH_WARNING_INTERVAL_IN_MINUTES = 5
export const ACCESS_TOKEN_KEY = 'access_token'
export const ID_COLUMN_WIDTH = 45
export const ROLE_LOCAL_STORAGE = 'role'
export const FULLNAME_LOCAL_STORAGE = 'fullname'