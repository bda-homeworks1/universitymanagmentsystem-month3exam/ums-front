import { RouteObject } from "react-router-dom";
import { ReactNode } from "react";

type RouteObjectAlias = Omit<RouteObject, 'children'>

export type MenuItem = RouteObjectAlias & {
    label: string,
    path: string,
    isVisibleMenu?: false,
    icon: ReactNode,
    children?: Array<(RouteObjectAlias & MenuItem)>  
}