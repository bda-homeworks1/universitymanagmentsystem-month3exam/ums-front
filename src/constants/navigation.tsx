import { lazy } from 'react'
import { MenuItem } from '@/constants/types'
import Icons, { IconsType } from '@/components/lib/Icons'
import { useStores } from '@/store'
import { AuthStore, LoginRequestResponse, Role, RoleMappings, authStore } from '@/store/stores/AuthStore'

const Main = lazy(async () => import('@/components/layouts/Main'))
const Login = lazy(async () => import('@/pages/Login'))
const Home = lazy(async () => import('@/pages/Home'))
const Teachers = lazy(async () => import('@/pages/Teachers'))
const Students = lazy(async () => import('@/pages/Students'))
const Admins = lazy(async () => import('@/pages/Admins'))
const Groups = lazy(async () => import('@/pages/Groups'))

const PAGE_HOME_PATH = '/'
const PAGE_LOGIN_PATH = '/login'
const PAGE_TEACHERS_PATH = '/teachers'
const PAGE_STUDENTS_PATH = '/students'
const PAGE_SETTINGS_PATH = '/settings'
const PAGE_GROUPS_PATH = '/groups'
const PAGE_ADMINS_PATH = `${PAGE_SETTINGS_PATH}/admins`


export const PAGE_HOME: MenuItem = {
  label: 'Ana Səhifə',
  path: PAGE_HOME_PATH,
  element: <Home />,
  icon: <Icons icon={IconsType.HomeOutlined} />,
}

export const PAGE_TEACHERS: MenuItem = {
  label: 'Müəllimlər',
  path: PAGE_TEACHERS_PATH,
  element: <Teachers />,
  icon: <Icons icon={IconsType.Teachers} />,
}

export const PAGE_STUDENTS: MenuItem = {
  label: 'Tələbələr',
  path: PAGE_STUDENTS_PATH,
  element: <Students />,
  icon: <Icons icon={IconsType.Teachers} />,
}

export const PAGE_GROUPS: MenuItem = {
  label: 'Qruplar',
  path: PAGE_GROUPS_PATH,
  element: <Groups />,
  icon: <Icons icon={IconsType.Teachers} />,
}

export const PAGE_ADMINS: MenuItem = {
  label: 'Adminlər',
  path: PAGE_ADMINS_PATH,
  element: <Admins />,
  icon: <Icons icon={IconsType.Teachers} />,
}

export const PAGE_SETTINGS: MenuItem = {
  label: 'Tənzimləmələr',
  path: PAGE_SETTINGS_PATH,
  icon: <Icons icon={IconsType.Teachers} />,
  children: [PAGE_ADMINS],
}
export const PAGE_LOGIN: MenuItem = {
  path: PAGE_LOGIN_PATH,
  element: <Login />,
  icon: <></>,
  label: ''
}

// export const SETTINGS_ADMIN_SUB = [PAGE_USERS, PAGE_CENTERS, PAGE_DISCOUNTS, PAGE_EMAIL]
// export const SETTINGS_MODERATOR_SUB = [PAGE_USERS, PAGE_CENTERS]

// export const PAGE_SETTINGS = (role: Role): MenuItem => ({
//   label: 'Tənzimləmələr',
//   path: PAGE_SETTINGS_PATH,
//   icon: <AiFillSetting />,
//   children: Role.Moderator === role ? SETTINGS_MODERATOR_SUB : SETTINGS_ADMIN_SUB,
// })

export const PAGES_ADMIN = [PAGE_HOME, PAGE_TEACHERS, PAGE_STUDENTS,
  PAGE_GROUPS, PAGE_SETTINGS]
export const PAGES_TEACHER = [
  PAGE_HOME
]
export const PAGES_STUDENT = [
    PAGE_HOME,
    PAGE_GROUPS
  ]

export const PAGES: Record<Role, MenuItem[]> = {
    [Role.ADMIN]: PAGES_ADMIN,
    [Role.TEACHER]: PAGES_TEACHER,
    [Role.STUDENT]: PAGES_STUDENT,
    
}

// export const PAGES = [
//     {
//         path: '/',
//         element: <Main/>,
//         children: [
//             authStore.data?.role===RoleMappings[Role.Admin]?PAGES_ADMIN:authStore.data.role===RoleMappings[Role.Teacher]?PAGES_TEACHER:PAGES_STUDENT
//         ],
//     },
//     PAGE_LOGIN,
//     //PAGE_NOT_FOUND
// ]

