export const COLOR_BLUE = '#0088FE';
export const COLOR_RED = '#ef233c'

export const CHART_COLORS = [COLOR_BLUE, '#00C49F', '#FFBB28', '#FF8042', COLOR_RED]