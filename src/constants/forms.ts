import { Sex, SexMappings, StatusMappings, Status, StatusMappingsForCustomers } from '@/store/types'
import { Role, RoleMappings } from '@/store/stores/AuthStore'
import { Rule } from 'antd/lib/form'

export const FORM_PAGE_COL_WIDTH = 8
export const REQUIRED_FIELD = 'Məcburi xanadır'
export const REQUIRED_FIELD_EMAIL = 'Zəhmət olmasa emailinizi yazın'
export const REQUIRED_FIELD_PASSWORD = 'Zəhmət olmasa parolunuzu yazın'
export const REQUIRED_FIELD_EMAIL_ERROR = 'Zəhmət olmasa email yazın'
export const PLACE_HOLDER_ENDING = 'daxil edin'
export const FORM_SAVE_TEXT = 'Yadda Saxla'
export const FORM_CONFIRM = 'Təsdiq et'
export const FORM_CANCEL = 'İmtina et'
export const FORM_NUMBER_INPUT_MASK = '+000 (00) 000 00 00'
export const FORM_UPLOAD_IMAGE_TEXT = 'Şəkil yüklə'
export const FORM_UPLOAD_ACCEPT = 'image/png, image/jpeg'
export const IMG_CROP_TITLE_TEXT = 'Şəkili redaktə et'
export const REQUIRED_FIELD_FILE = [{ required: true, message: REQUIRED_FIELD }]

export const EMAIL_ADDRESS_PLACEHOLDER = `Elektron poçt ${PLACE_HOLDER_ENDING}`

export const CENTER_NAME_REQUIRED_FILE = [
  { min: 3, max: 3, message: 'Mərkəzin qısa adı 3 simvoldan ibarət olmalıdır' },
  { required: true, message: REQUIRED_FIELD },
]

export const REQUIRED_FIELD_RULES: Rule[] = [
  { required: true, message: REQUIRED_FIELD },
  { whitespace: true, message: REQUIRED_FIELD },
]

export const REQUIRED_FIELD_EMAIL_RULES: Rule[] = [
  { required: true, message: REQUIRED_FIELD },
  { type: 'email', message: REQUIRED_FIELD_EMAIL_ERROR },
]

export const REQUIRED_FIELD_PASSWORD_RULES: Rule[] = [
  { required: true, message: REQUIRED_FIELD_PASSWORD },
]

export const REQUIRED_NUMBER_FIELD_RULES = [{ required: true, message: REQUIRED_FIELD }]

export const REQUIRED_INPUT_MASK = [
  {
    required: true,
    pattern: /^\+([0-9]{1,3}) \([0-9]{2}\) [0-9]{3} [0-9]{2} [0-9]{2}$/,
    message: 'Nömrə tam şəkildə daxil edilməlidir',
  },
]

export const GENDERS = [
  { key: 0, label: SexMappings[Sex.M], value: Sex.M },
  { key: 1, label: SexMappings[Sex.F], value: Sex.F },
]


export const ROLES = [
  { key: 1, label: RoleMappings[Role.ADMIN], value: Role.ADMIN },
  { key: 2, label: RoleMappings[Role.TEACHER], value: Role.TEACHER },
  { key: 3, label: RoleMappings[Role.STUDENT], value: Role.STUDENT },

]

export const STATUSES = [
  { key: 0, label: StatusMappings[Status.Active], value: Status.Active },
  { key: 1, label: StatusMappings[Status.Inactive], value: Status.Inactive },
]

export const STATUSESCUSTOMERS = [
  { key: 0, label: StatusMappingsForCustomers[Status.Active], value: Status.Active },
  { key: 1, label: StatusMappingsForCustomers[Status.Inactive], value: Status.Inactive },
]