const AutoVersion = require('auto-version-js')
const version = AutoVersion.parse(AutoVersion.getVersion())

const updateFunction = () => {
  if (version.patch + 1 > 10) {
    version.minor++
    version.patch = 0
  } else {
    version.patch++
  }

  if (version.minor + 1 > 10) {
    version.major++
    version.minor = 0
  }

  AutoVersion.setVersion(AutoVersion.stringify(version))
  console.log('Version updated to:', AutoVersion.getVersion())
}

updateFunction()