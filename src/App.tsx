import Routing from './Routing'
import { StoreProvider } from './store'
import RootStore from './store/classes/RootStore'

const store = new RootStore()

function App() {
  return (
    <StoreProvider store={store}>
      <Routing />
    </StoreProvider>
  )
}

export default App