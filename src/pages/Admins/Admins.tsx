import Breadcrumbs from '@/components/lib/Breadcrumbs/Breadcrumbs'
import { PAGE_ADMINS, PAGE_SETTINGS } from '@/constants'
import { observer } from 'mobx-react'
import style from './Admins.module.scss'
import { useStores } from '@/store'
import { useNavigate } from 'react-router-dom'
import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import FormModal, { IFormModalHandle } from '@/components/lib/FormModal'
import { IAdmin } from '@/store/stores/AdminsStore'
import { getAdminsColumns } from './constants'
import { Button, Modal, Table } from 'antd'
import AdminsForm from '@/components/lib/AdminFormModal/AdminsForm'

const Admins = () => {
  const { adminsStore } = useStores()
  const navigate = useNavigate()
  const formRef = useRef<IFormModalHandle>(null)

  const onViewClick = useCallback(
    (data: IAdmin) => {
      return navigate(`${PAGE_ADMINS.path}/${data.id}`)
    },
    [navigate]
  )

  const [isModalOpen, setIsModalOpen] = useState(false)

  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleOk = () => {
    setIsModalOpen(false)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
  }
  const onSubmit = useCallback(
    async (data: unknown) => {
      const inter = data as IAdmin
      // if (!usersStore.isActiveUserAdmin) {
      //   inter.branchId = usersStore.activeUser.departmentId
      // }
      // return null
    },
    [adminsStore]
  )

  const onEdit = useCallback(
    async (id: number, data: unknown) => {
      const inter = data as IAdmin
      // if (!usersStore.isActiveUserAdmin) {
      //   inter.branchId = usersStore.activeUser.departmentId
      // }
      // return customersStore.update(id, inter)
      // return null
    },
    // [customersStore, usersStore.activeUser.departmentId, usersStore.isActiveUserAdmin]
    [adminsStore]
  )

  const onEditClick = useCallback((data: IAdmin) => {
    formRef.current?.setEdit(data)
  }, [])

  useEffect(() => {
    adminsStore.getAdmins()
  }, [adminsStore])

  const columns = useMemo(
    () => getAdminsColumns(onViewClick, onEditClick),
    [onViewClick, onEditClick]
  )

  return (
    <div className={style.container}>
      <Modal title='Register Admin' open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer ={ null} >
        <AdminsForm isEditing={false}/>
      </Modal>

      <Breadcrumbs pageWithoutPath={PAGE_SETTINGS} pageSecond={PAGE_ADMINS} addFunction={showModal} />
      <Table columns={columns} dataSource={adminsStore.admins} loading={adminsStore.isLoading} />
    </div>
  )
}

export default observer(Admins)
