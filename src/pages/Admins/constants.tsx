import { Button, Input, Rate, Select, SelectProps, Space, Switch, Tag, Tooltip } from 'antd'
import { AiFillEdit, AiFillEye } from 'react-icons/ai'

// import { Status, StatusColorMappings, StatusMappings } from '@/store/types'
import { formatDate, FullNameAdder } from '@/utils'
import { ID_COLUMN_WIDTH } from '@/constants'
import { FilterableColumns } from '@/components/lib/Table/types'
// import DateRange from '@/components/lib/DateRange'
// import TotalRate from '@/components/lib/TotalRate'
import { capitalize } from '@/utils/strings'
import { IAdmin } from '@/store/stores/AdminsStore'

export const getAdminsColumns = (
  onViewClick: (record: IAdmin) => void,
  onEditClick: (record: IAdmin) => void,
//   onChangeStatus: (record: IAdmin) => void,
): FilterableColumns<FullNameAdder<IAdmin>> => [
    // {
    //   title: '',
    //   dataIndex: '',
    // //   width: ID_COLUMN_WIDTH,
    //   width: '20px',
    //   key: '',
    //   children: [],
    // },

    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      width: '100px',
      filterKey: 'id',
      element: <Input type='text' />,
    },
    {
      title: 'Ad',
      dataIndex: 'name',
      key: 'name',
      filterKey: 'name',
      width: '100px',
      element: <Input type='text' />,
      render: (value: string) => { if(value!== undefined ){
         return `${capitalize(value)}`}
         return  <Tag color='black'>Məlumat Yoxdur</Tag>
    },
      children: [],
    },
    {
        title: 'Soyad',
        dataIndex: 'surname',
        key: 'surname',
        filterKey: 'surname',
        width: '100px',
        element: <Input type='text' />,
        render: (value: string) => { if(value!== undefined ){
           return `${capitalize(value)}`}
           return  <Tag color='black'>Məlumat Yoxdur</Tag>
      },
      },
      {
        title: 'username',
        dataIndex: 'username',
        key: 'username',
        filterKey: 'username',
        width: '100px',
        element: <Input type='text' />,
      },
      {
        title: 'password',
        dataIndex: 'password',
        key: 'password',
        filterKey: 'password',
        width: '100px',
        element: <Input type='text' />,
      },
    // {
    //   title: 'Əlaqə nömrəsi',
    //   dataIndex: 'phone',
    //   key: 'phone',
    //   filterKey: 'phone',
    //   element: <Input type='text' />,
    //   children: [],
    // },
    // {
    //   title: 'Kateqoriya',
    //   dataIndex: 'subject',
    //   key: 'subject',
    //   width: '100px',
    //   filterKey: 'subject',
    //   element: <Input type='text' />,
    //   children: [],
    //   render: (data) => {
    //     if (data === null || data === undefined) {
    //       return <Tag color='orange'>Məlumat Yoxdur</Tag>
    //     }
    //     return <div>{data}</div>
    //   },
    // },
    // {
    //   title: 'Qeydiyyat tarixi',
    //   dataIndex: 'created_at',
    //   key: 'created_at',
    //   filterKey: 'created_at',
    //   element: <DateRange />,
    //   render: (data: Date) => {
    //     if (data === null) {
    //       return <Tag color='orange'>Məlumat Yoxdur</Tag>
    //     }
    //     return <Tag>{formatDate(data)}</Tag>
    //   },
    //   children: [],
    // },
    {
      title: 'Əməliyyatlar',
      dataIndex: '',
      key: 'action',
      width: '100px',
      fixed: 'right',
      children: [],
      render: (data: IAdmin) => (
        <>
          <Space>
            <>
              <Tooltip placement='top' title='Statusu dəyiş'>
                {/* <Switch
                  size='small'
                  checked={Boolean(data.status)}
                  onChange={() => onChangeStatus(data)}
                /> */}
              </Tooltip>
              <Tooltip placement='top' title='Redaktə et'>
                <Button size='small' icon={<AiFillEdit />} onClick={() => onEditClick(data)} />
              </Tooltip>
              <Tooltip placement='top' title='Detallı baxış'>
                <Button size='small' icon={<AiFillEye />} onClick={() => onViewClick(data)} />
              </Tooltip>
            </>
          </Space>
        </>
      ),
    },
  ]
