import Breadcrumbs from '@/components/lib/Breadcrumbs/Breadcrumbs'
import { PAGE_GROUPS } from '@/constants'
import { observer } from 'mobx-react'
import style from './Groups.module.scss'
import { useStores } from '@/store'
import { useNavigate } from 'react-router-dom'
import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import FormModal, { IFormModalHandle } from '@/components/lib/FormModal'
import { getGroupsColumns } from './constants'
import { Button, Modal, Table } from 'antd'
import AdminsForm from '@/components/lib/AdminFormModal/AdminsForm'
import { IGroup } from '@/store/stores/GroupsStore'
import GroupsForm from '@/components/lib/GroupFormModal/AdminsForm'

const Groups = () => {
  const { groupsStore } = useStores()
  const navigate = useNavigate()
  const formRef = useRef<IFormModalHandle>(null)

  const onViewClick = useCallback(
    (data: IGroup) => {
      return navigate(`${PAGE_GROUPS.path}`)
    },
    [navigate]
  )

  const [isModalOpen, setIsModalOpen] = useState(false)

  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleOk = () => {
    setIsModalOpen(false)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
  }
  const onSubmit = useCallback(
    async (data: unknown) => {
      const inter = data as IGroup
    },
    [groupsStore]
  )

  const onEdit = useCallback(
    async (id: number, data: unknown) => {
      const inter = data as IGroup

    },
    [groupsStore]
  )

  const onEditClick = useCallback((data: IGroup) => {
    formRef.current?.setEdit(data)
  }, [])

  useEffect(() => {
    groupsStore.getGroups()
  }, [groupsStore])

  const columns = useMemo(
    () => getGroupsColumns(onViewClick, onEditClick),
    [onViewClick, onEditClick]
  )

  return (
    <div className={style.container}>
      <Modal title='Register Groups' open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer ={ null} >
        <GroupsForm isEditing={false}/>
      </Modal>

      <Breadcrumbs pageSecond={PAGE_GROUPS} addFunction={showModal} />
      <Table columns={columns} dataSource={groupsStore.groups} loading={groupsStore.isLoading} />
    </div>
  )
}

export default observer(Groups)
