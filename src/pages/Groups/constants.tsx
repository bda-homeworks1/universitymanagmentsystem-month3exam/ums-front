import { Button, Input, Rate, Select, SelectProps, Space, Switch, Tag, Tooltip } from 'antd'
import { AiFillEdit, AiFillEye } from 'react-icons/ai'

// import { Status, StatusColorMappings, StatusMappings } from '@/store/types'
import { formatDate, FullNameAdder } from '@/utils'
import { ID_COLUMN_WIDTH } from '@/constants'
import { FilterableColumns } from '@/components/lib/Table/types'
// import DateRange from '@/components/lib/DateRange'
// import TotalRate from '@/components/lib/TotalRate'
import { capitalize } from '@/utils/strings'
import { IGroup } from '@/store/stores/GroupsStore'

export const getGroupsColumns = (
  onViewClick: (record: IGroup) => void,
  onEditClick: (record: IGroup) => void,
//   onChangeStatus: (record: IAdmin) => void,
): FilterableColumns<FullNameAdder<IGroup>> => [
    // {
    //   title: '',
    //   dataIndex: '',
    // //   width: ID_COLUMN_WIDTH,
    //   width: '20px',
    //   key: '',
    //   children: [],
    // },

    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      width: '100px',
      filterKey: 'id',
      element: <Input type='text' />,
    },
    {
      title: 'Ad',
      dataIndex: 'name',
      key: 'name',
      filterKey: 'name',
      width: '100px',
      element: <Input type='text' />,
      render: (value: string) => { if(value!== undefined ){
         return `${capitalize(value)}`}
         return  <Tag color='black'>Məlumat Yoxdur</Tag>
    },
    },
  
   
    {
      title: 'Əməliyyatlar',
      dataIndex: '',
      key: 'action',
      width: '100px',
      fixed: 'right',
      children: [],
      render: (data: IGroup) => (
        <>
          <Space>
            <>
              <Tooltip placement='top' title='Statusu dəyiş'>
                {/* <Switch
                  size='small'
                  checked={Boolean(data.status)}
                  onChange={() => onChangeStatus(data)}
                /> */}
              </Tooltip>
              <Tooltip placement='top' title='Redaktə et'>
                <Button size='small' icon={<AiFillEdit />} onClick={() => onEditClick(data)} />
              </Tooltip>
              <Tooltip placement='top' title='Detallı baxış'>
                <Button size='small' icon={<AiFillEye />} onClick={() => onViewClick(data)} />
              </Tooltip>
            </>
          </Space>
        </>
      ),
    },
  ]
