import Breadcrumbs from '@/components/lib/Breadcrumbs/Breadcrumbs';
import { PAGE_TEACHERS } from '@/constants';
import { observer } from 'mobx-react';
import React, { ReactElement, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import style from './Teachers.module.scss'
import { useStores } from '@/store';
import { fullNameParser } from '@/utils';
import { getTeachersColumns } from './constants';
import { ITeacher } from '@/store/stores/TeachersStore';
import { useNavigate } from 'react-router-dom';
import { IFormModalHandle } from '@/components/lib/FormModal';
import { Modal, Table } from 'antd';
import TeacherForm from '@/components/lib/TeacherFormModal/TeacherForm';



const Teachers: React.FC = (): ReactElement => {
    const {teachersStore} = useStores()
    const navigate = useNavigate()
    const formRef = useRef<IFormModalHandle>(null)
    const [isOpenModal, setIsModalOpen ] = useState(false)
 

    const showModal = () => {
        setIsModalOpen(true);
    }

    const handleCancel = () => {
        setIsModalOpen(false)
    }

    const onViewClick = useCallback((data: ITeacher) => {
        return navigate(`${PAGE_TEACHERS.path}/${data.id}`)
    }, [navigate])

    const onEditClick = useCallback((data: ITeacher) => {
        formRef.current?.setEdit(data)
    }, [])

    useEffect(() => {
        teachersStore.getTeachers()
    }, [teachersStore.createTeacher])

    const columns = useMemo(() => getTeachersColumns(onViewClick, onEditClick), [onViewClick, onEditClick])

    return (
        <div className={style.container}>
            <Breadcrumbs pageFirst={PAGE_TEACHERS} addFunction={showModal}/>
            <Table columns={columns} dataSource={teachersStore.teachers} loading = {teachersStore.isLoading} />
            <Modal title="Register Teacher" open={isOpenModal} onCancel={handleCancel} footer={null}>
                <TeacherForm isEditing={false}/>
            </Modal>
        </div>
    );
}

export default observer(Teachers);
