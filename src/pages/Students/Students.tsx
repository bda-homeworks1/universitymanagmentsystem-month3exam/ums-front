import Breadcrumbs from '@/components/lib/Breadcrumbs/Breadcrumbs'
import { PAGE_STUDENTS } from '@/constants'
import { observer } from 'mobx-react'
import React, { ReactElement, useCallback, useEffect, useMemo, useRef, useState } from 'react'
import style from './Students.module.scss'
import { useStores } from '@/store'
import { useNavigate } from 'react-router-dom'
import { IFormModalHandle } from '@/components/lib/FormModal'
import { IStudent } from '@/store/stores/StudentsStore'
import { getStudentsColumns } from './constants'
import { Modal, Table } from 'antd'
import StudentForm from '@/components/lib/StudentFormModal/StudentForm'
import { size } from 'lodash'


const Students: React.FC = (): ReactElement => {
  const { studentsStore, groupsStore } = useStores()
  const navigate = useNavigate()
  const formRef = useRef<IFormModalHandle>(null)
  const [isOpen, setIsOpen] = useState(false)

  const showModal = () => {
    groupsStore.getGroups()
    setIsOpen(true)
  }

  const handleClose = () => {
    setIsOpen(false)
  }

  const onViewClick = useCallback(
    (data: IStudent) => {
      return navigate(`${PAGE_STUDENTS.path}/${data.id}`)
    },
    [navigate]
  )

  const onEditClick = useCallback((data: IStudent) => {
    formRef.current?.setEdit(data)
  }, [])

  useEffect(() => {
    studentsStore.getStudents()
  }, [studentsStore])

  const columns = useMemo(
    () => getStudentsColumns(onViewClick, onEditClick),
    [onViewClick, onEditClick]
  )

  return (
    <div className={style.container}>
      <Breadcrumbs pageFirst={PAGE_STUDENTS} addFunction={showModal} />
      <Table columns={columns} dataSource={studentsStore.students} loading={studentsStore.isLoading}/>
      <Modal open={isOpen} onCancel={handleClose} footer={null} >
        <StudentForm isEditing={false} />
      </Modal>
    </div>
  )
}

export default observer(Students)
