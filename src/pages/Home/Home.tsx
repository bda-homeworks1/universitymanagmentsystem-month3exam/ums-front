import Breadcrumbs from '@/components/lib/Breadcrumbs/Breadcrumbs'
import { observer } from 'mobx-react'
import React, { ReactElement } from 'react'

const Home: React.FC = (): ReactElement => {
  return (
    <Breadcrumbs showButton={false}/>
  )
}

export default observer(Home)
