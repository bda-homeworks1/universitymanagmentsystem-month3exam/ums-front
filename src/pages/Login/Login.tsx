import { Button, Typography, Form, Input } from 'antd'
import { Suspense, useEffect } from 'react'
import style from './Login.module.scss'
import logo from '@/assets/logo_transparent.png'
import {
  REQUIRED_FIELD_EMAIL_RULES,
  REQUIRED_FIELD_PASSWORD_RULES,
} from '@/constants/forms'
import { PAGE_HOME } from '@/constants'
import { useNavigate } from 'react-router-dom'
import { useStores } from '@/store'
import { LoginRequest } from '@/store/stores/AuthStore'
import Loader from '@/components/lib/Spinner'

const { Title } = Typography
const { Item, useForm } = Form

const Login = () => {
  const { authStore } = useStores()

  const [form] = useForm()

  const navigate = useNavigate()

  useEffect(() => {

    if (authStore.isAuthenticated) {
      navigate(PAGE_HOME.path)
    }
  }, [navigate,authStore.isAuthenticated])


 

  const onFinish = (values: LoginRequest) => {
    authStore
      .login(values)
      .then(() => {
        navigate(PAGE_HOME.path)
      })
      .catch(() => {
        console.log('Xəta baş verdi')
      })
  }

  // const loading = useMemo(
  //   () => authStore.isLoading,
  //   [authStore.isLoading]
  // )

  return (
    <Suspense fallback={<Loader />}>
      <div className={style.container}>
      
        <div className={style.form}>
        <div className={style.figure}>
          <img className={style.image} alt={'logo'} src={logo} />
        </div>
          <div className={style.title}>
            <Title level={3}>Daxil ol</Title>
          </div>
          
          <div className={style.input}>
            <Form form={form} name='basic' initialValues={{ remember: true }} layout='vertical' onFinish={onFinish}>
              <Item name='username' label={'username'}>
                <Input
                  className={style.input}
                  placeholder='Zəhmət olmasa username yazın'
                />
              </Item>

              <Item name='password' rules={REQUIRED_FIELD_PASSWORD_RULES} label={'Şifrə'}>
                <Input.Password className={style.input} placeholder='Zəhmət olmasa şifrə yazın' />
              </Item>

              <Item>
                <Button className={style.button} type='primary' htmlType='submit'>
                  Daxil ol
                </Button>
              </Item>
            </Form>
          </div>
        </div>
      </div>
    </Suspense>
  )
}

export default Login