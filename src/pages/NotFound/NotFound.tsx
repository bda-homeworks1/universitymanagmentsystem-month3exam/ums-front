import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from 'antd'
import style from './Notfound.module.scss'
import image from '@/assets/notfound.jpg'

const NotFound = () => {
  const navigate = useNavigate()
  const navigateHome = () => {
    navigate('/')
  }

  return (
    <div className={style.notFoundContainer}>
      <div className={style.notfound}>
        <div className={style.allMess}>
          <h1 className={style.errMess}>404 Not Found</h1>
          <span className={style.errMessAz}>Səhifə tapılmadı</span>
          <Button className={style.submitBtn} onClick={navigateHome} type={'primary'}>
            Ana Səhifə
          </Button>
        </div>
      </div>
      <div className={style.img}>
        <img src={image} alt='img' />
      </div>
    </div>
  )
}

export default NotFound
