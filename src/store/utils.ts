export class ModelMapper<T> {
  _propertyMapping: Record<
    string,
    {
      sourceProperty: string
      forwards?: (s: any) => any
      backwards?: (s: any) => any
    }
  >

  _target: any

  constructor(Type: new () => T, obj?: any) {
    this._target = new Type()

    this._propertyMapping = this._target.constructor._propertyMap

    if (obj !== undefined) {
      Object.assign(this._target, obj)
    }
  }

  map(source: any): T {
    Object.keys(this._target).forEach((key) => {
      const mappedKey =
        this._propertyMapping !== undefined ? this._propertyMapping[key]?.sourceProperty : undefined

      const forwards =
        this._propertyMapping !== undefined ? this._propertyMapping[key]?.forwards : undefined

      if (mappedKey !== undefined) {
        this._target[key] = forwards !== undefined ? forwards(source[mappedKey]) : source[mappedKey]
      } else {
        this._target[key] = source[key]
      }
    })

    return this._target
  }

  reverseMap(): any {
    const mapped: any = {}

    Object.keys(this._target).forEach((key) => {
      if (this._target === undefined) return
      if (this._target[key] === undefined || this._target[key] === null || this._target[key] === '')
        return

      const mappedKey =
        this._propertyMapping !== undefined ? this._propertyMapping[key]?.sourceProperty : undefined
      const backwards =
        this._propertyMapping !== undefined ? this._propertyMapping[key]?.backwards : undefined

      if (mappedKey !== undefined) {
        mapped[mappedKey] =
          backwards !== undefined ? backwards(this._target[key]) : this._target[key]
      } else {
        mapped[key] = backwards !== undefined ? backwards(this._target[key]) : this._target[key]
      }
    })

    return mapped
  }

  mapErrors(source: Record<string, string[]>): Record<keyof T, string[]> {
    Object.keys(this._target).forEach((key) => {
      const mappedKey =
        this._propertyMapping !== undefined ? this._propertyMapping[key]?.sourceProperty : undefined
      if (mappedKey !== undefined) {
        this._target[key] = source[mappedKey]
      } else {
        this._target[key] = source[key]
      }
    })

    return this._target
  }
}