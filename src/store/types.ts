export enum Sex {
  F,
  M,
}

export enum Status {
  Inactive,
  Active,
}

export const SexMappings: Record<Sex, string> = {
  [Sex.F]: 'Qadın',
  [Sex.M]: 'Kişi',
}

export const SexMappingsEnglish: Record<Sex, string> = {
  [Sex.F]: 'Female',
  [Sex.M]: 'Male',
}

export const StatusMappings: Record<Status, string> = {
  [Status.Active]: 'Aktiv',
  [Status.Inactive]: 'Deaktiv',
}

export const StatusMappingsForCustomers: Record<Status, string> = {
  [Status.Active]: 'Aktiv',
  [Status.Inactive]: 'Qadağan olunmuş',
}

export interface ServerResponse<T> {
  data: T
  total: number
}

interface TreeElement {
  key: number
  value: number
  title: string
}

export type Tree = TreeElement[]