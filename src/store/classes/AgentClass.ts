import { ModelMapper } from '../../store/utils'
import baseService from '../../store/baseService'
import { ServerResponse } from '../../store/types'
import { AxiosError } from 'axios'

export class AgentClass<T> {
  url: string
  Class: new () => T

  constructor(url: string, Type: new () => T) {
    this.url = `/${url}`
    this.Class = Type
  }

  handle400Error = (e: AxiosError) => {
    const data = e.response?.data as { error: Record<string, string[]> }
    return Object.assign(new Error('form error'), {
      errors: new ModelMapper(this.Class).mapErrors(data.error),
    })
  }

  getPassableData = (data: Partial<T>) => {
    const mappableData = new this.Class()
    Object.assign(mappableData as unknown as object, data)
    return new ModelMapper(this.Class, mappableData).reverseMap()
  }

  getResultData = (data: Partial<T>): T => {
    return new ModelMapper(this.Class).map(data)
  }

  getAll = async (params: Partial<T>) => {
    const parsed = this.getPassableData(params)

    const res = await baseService.get<ServerResponse<T[]>>(`${this.url}`, {
      params: { ...parsed },
    })
    return { data: res.data.data.map(this.getResultData), count: res.data.total }
  }

  getSingle = async (id: number) => {
    const res = await baseService.get<T[]>(`${this.url}/single/${id}`)
    if (Array.isArray(res.data)) {
      return { data: this.getResultData(res.data[0]) }
    }

    return { data: this.getResultData(res.data) }
  }

  create = async (data: Partial<T>) => {
    const parsed = this.getPassableData(data)

    try {
      const res = await baseService.post<{data: T}>(`${this.url}/store`, parsed)
      return { data: this.getResultData(res.data.data) }
    } catch (e) {
      const axoError = e as AxiosError
      if (axoError.response?.status === 400) {
        throw this.handle400Error(axoError)
      }
      throw e
    }
  }

  update = async (id: number, data: Partial<T>) => {
    const parsed = this.getPassableData(data)

    try {
      const res = await baseService.put<{data: T}>(`${this.url}/update/${id}`, parsed)
      return { data: this.getResultData(res.data.data) }
    } catch (e: unknown) {
      const axoError = e as AxiosError
      if (axoError.response?.status === 400) {
        throw this.handle400Error(axoError)
      }
      throw e
    }
  }

  delete = async (id: number) => {
    await baseService.delete(`${this.url}/delete/${id}`)
  }
}