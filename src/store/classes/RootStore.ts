import { configure, makeAutoObservable, reaction } from 'mobx'
import AdminsStore from '../stores/AdminsStore'
import TeachersStore from '../stores/TeachersStore'
import StudentsStore from '../stores/StudentsStore'
import { AuthStore } from '../stores/AuthStore'
import GroupsStore from '../stores/GroupsStore'

configure({
  enforceActions: 'never',
})

export default class RootStore {
  adminsStore: AdminsStore
  teachersStore: TeachersStore
  studentsStore: StudentsStore
  groupsStore: GroupsStore
  authStore: AuthStore


  constructor() {
    makeAutoObservable(this)
    this.adminsStore = new AdminsStore()
    this.teachersStore = new TeachersStore()
    this.studentsStore = new StudentsStore()
    this.groupsStore = new GroupsStore()
    this.authStore = new AuthStore()


    reaction(
      () => [
        this.authStore.isAuthenticated
      ],
      () => {
        if (this.authStore.isAuthenticated) {
          this.adminsStore.loadItems()
          this.teachersStore.loadItems()
          this.studentsStore.loadItems()
          this.groupsStore.loadItems()
        }

        // this.adminsStore.load()
        // this.teachersStore.load()
        // this.studentsStore.load()
    
      }
    )
  }
}