import { action, computed, makeObservable, observable } from 'mobx'
import { openErrorNotification, showErrorMessage, showSuccessMessage } from '@/utils'
import { SingleAgentClass } from '@/store/classes/SingleAgentClass'

class SingleFetchableStore<T> {
  service: SingleAgentClass<T>
  item: T = {} as T
  label: string
  isFetchLoading = false
  isUpdateLoading = false

  constructor(service: SingleAgentClass<T>, label: string) {
    makeObservable(this, {
      item: observable,
      isFetchLoading: observable,
      isUpdateLoading: observable,
      isLoading: computed,
      load: action,
      update: action,
    })
    this.service = service
    this.label = label
  }

  load = () => {
    this.isFetchLoading = true
    this.service.getSingle()
      .then(({ data }) => {
        this.item = data
      })
      .catch((e) => {
        openErrorNotification(this.label, e)
        throw e
      })
      .finally(() => {
        this.isFetchLoading = false
      })
  }

  update = (data: Partial<T>) => {
    this.isUpdateLoading = true
    this.service.create(data)
      .then(() => {
        showSuccessMessage()
        this.load()
      })
      .catch((e) => {
        showErrorMessage()
        throw e
      })
      .finally(() => {
        this.isUpdateLoading = false
      })
  }

  get isLoading () {
    return this.isFetchLoading ||
    this.isUpdateLoading
  }
 }

export default SingleFetchableStore