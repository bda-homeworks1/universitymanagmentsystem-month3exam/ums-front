import {Tree } from '../../store/types'
import { AgentClass } from '../../store/classes/AgentClass'
import { action, computed, makeObservable, observable } from 'mobx'
import {
  openErrorNotification,
  showErrorMessage,
  showSuccessMessage,
} from '../../utils'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

export default class FetchableStore<Visible> {
  service: AgentClass<Visible>
  items: Visible[] = []
  tree: Tree = []
  lastFetched: Date | undefined

  entityName: string



  isItemsLoading = false
  isCreateLoading = false
  isUpdateLoading = false
  isDeleteLoading = false
  isTreeLoading = false
  isExcelDownloading = false

  timer: NodeJS.Timer | undefined

  count = 0
  filters: Partial<Visible> = {}

  constructor(service: AgentClass<Visible>, entityName: string) {
    makeObservable(this, {
      items: observable,
      count: observable,
      filters: observable,
      load: action,
      loadItems: action,
      create: action,
      delete: action,
      update: action,
      setFilters: action,
      isLoading: computed,
      isItemsLoading: observable,
      isCreateLoading: observable,
      isUpdateLoading: observable,
      isDeleteLoading: observable,
      isTreeLoading: observable,
      lastFetched: observable,
      isExcelDownloading: observable,
      tree: observable,
      dispose: action,
      startTimer: action,
    })

    this.service = service
    this.entityName = entityName
  }

  startTimerAndLoad = (loadTree = true) => {
    this.load(loadTree)
    this.startTimer(loadTree)
  }

  startTimer = (loadTree = true) => {
    this.timer = setInterval(() => {
      const diff = dayjs().diff(dayjs(this.lastFetched), 'seconds')
      if (diff > 60 * 5) {
        this.load(loadTree)
      }
    }, 1000)
  }

  load = (tree = true) => {
    this.loadItems()
  }

  dispose = () => {
    this.items = []
    this.tree = []
    if (this.timer !== undefined) {
      return clearInterval(this.timer)
    }
  }

  loadItems = () => {
    this.isItemsLoading = true

    this.service
      .getAll(this.filters)
      .then(({ data, count }) => {
        this.lastFetched = new Date()
        this.items = data
        this.count = count
      })
      .catch((error) => {
        openErrorNotification(this.entityName, error)
      })
      .finally(() => {
        this.isItemsLoading = false
      })
  }

  create = async (data: Partial<Visible>) => {
    this.isCreateLoading = true

    return await this.service
      .create(data)
      .then(({ data }) => {
        showSuccessMessage()
        this.load()
        return data
      })
      .catch((e) => {
        showErrorMessage()
        throw e
      })
      .finally(() => {
        this.isCreateLoading = false
      })
  }

  delete = async (id: number) => {
    this.isDeleteLoading = true

    return await this.service
      .delete(id)
      .then(() => {
        this.load()
        showSuccessMessage()
      })
      .catch((e) => {
        showErrorMessage()
        throw e
      })
      .finally(() => {
        this.isDeleteLoading = false
      })
  }

  update = async (id: number, data: Partial<Visible>) => {
    this.isUpdateLoading = true

    return await this.service
      .update(id, data).then(() => {
        showSuccessMessage()
        this.load()
      })
      .catch((e) => {
        showErrorMessage()
        throw e
      })
      .finally(() => {
        this.isUpdateLoading = false
      })
  }



  setFilters = (filters: Partial<Visible>) => {
    this.filters = Object.fromEntries(Object.entries({ ...this.filters, ...filters }).map(([key, value]) =>
      [key.trim(), typeof value === 'string' ? value.trim() : value])) as unknown as Partial<Visible>
    this.loadItems()
  }

  clearFilters = () => {
    this.filters = {}
    this.loadItems()
  }

  get isLoading() {
    return (
      this.isItemsLoading || this.isCreateLoading || this.isUpdateLoading || this.isDeleteLoading
    )
  }
}
