import baseService from '@/store/baseService'
import { AgentClass } from '@/store/classes/AgentClass'

export class SingleAgentClass<T> extends AgentClass<T> {
  getSingle = async () => {
    const res = await baseService.get<T>(super.url)
    return { data: super.getResultData(res.data) }
  }

  create = async (data: Partial<T>) => {
    const res = await baseService.put(super.url, data)
    return { data: super.getResultData(res.data) }
  }
}