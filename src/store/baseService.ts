import axios, { AxiosRequestConfig } from 'axios'
import { ACCESS_TOKEN_KEY } from '../constants/storage'

const baseService = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL,
})

baseService.interceptors.request.use((config: AxiosRequestConfig) => {
  const token: string | undefined | null = localStorage.getItem(ACCESS_TOKEN_KEY)

  if (token == null && !window.location.href.includes('login')) {
    window.location.replace('/login')
    console.log("token==null" + token)

    return Promise.reject(new Error())
  }

  if (token != null) {
    console.log("token!==null, token= " + token)
    config.headers = {
      Authorization: `Bearer ${token}`,
    }
  }

  return config
})

baseService.interceptors.response.use((res) => res, async (res) => {

  if (res.response.status === 401) {
    console.log("401 lik, hisse")
    const oldValue = window.localStorage.getItem(ACCESS_TOKEN_KEY)
    window.localStorage.removeItem(ACCESS_TOKEN_KEY)
    const event = new StorageEvent('storage', {
      key: ACCESS_TOKEN_KEY,
      oldValue,
      newValue: null,
    });

    window.dispatchEvent(event)
  }

  return await Promise.reject(res)
})

export default baseService