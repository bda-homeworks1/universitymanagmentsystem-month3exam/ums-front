import { AgentClass } from "@/store/classes/AgentClass";
import { IGroup } from "./types";
import baseService from "@/store/baseService";

export class GroupsService extends AgentClass<IGroup>{
    constructor(){
        super('api/groups', IGroup)
    }

    getGroups = async() => {
        const res = await baseService.get('/api/groups')
        return res.data
    }

    createGroup = async(data: Partial<IGroup>) => {
        baseService.post('/api/group', data)
        return data
    }
    
}