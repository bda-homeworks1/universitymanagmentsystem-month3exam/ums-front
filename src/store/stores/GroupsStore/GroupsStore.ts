import FetchableStore from "@/store/classes/FetchableStore";
import { IGroup } from "./types";
import { GroupsService } from "./service";
import { PAGE_GROUPS } from "@/constants";
import { action, makeObservable } from "mobx";
import { showErrorMessage, showSuccessMessage } from "@/utils";

class GroupsStore extends FetchableStore<IGroup>{
    groups: IGroup[] = []

    constructor(){
        super(new GroupsService(), PAGE_GROUPS.label)
        makeObservable(this, {
            getGroups: action

        })
    }


    getGroups = () => {
        this.isItemsLoading = true;
        (this.service as GroupsService)
        .getGroups()
        .then((data) => {
            this.groups = data
            console.log(this.groups, 'storedaki groups')
        })
        .catch(() => showErrorMessage())
        .finally(() => {
            this.isItemsLoading = false;
        })
    }

    createGroup = (data: Partial<IGroup>) => {
        this.isUpdateLoading = true;
        (this.service as GroupsService)
        .createGroup(data)
        .then(() => {
            showSuccessMessage()
            this.load()
        })
        .catch(() => {
            showErrorMessage()
        })
        .finally(() => {
            this.isUpdateLoading = false
        })
    }

    // add = (data: IGroup) => {
    //     this.isUpdateLoading = true;
    //     (this.service as GroupsService)
    //       .add(data)
    //       .then(() => {
    //         showSuccessMessage()
    //         this.load()
    //       })
    //       .catch(() => {
    //         showErrorMessage()
    //       })
    //       .finally(() => {
    //         this.isUpdateLoading = false
    //       })
    //   }
}

export default GroupsStore