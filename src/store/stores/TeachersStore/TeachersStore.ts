import FetchableStore from "@/store/classes/FetchableStore";
import { ITeacher } from "./types";
import { TeachersService } from "./service";
import { PAGE_TEACHERS } from "@/constants";
import { action, makeObservable } from "mobx";
import { showErrorMessage, showSuccessMessage } from "@/utils";

class TeachersStore extends FetchableStore<ITeacher>{
    teachers: ITeacher[] = []
    constructor(){
        super(new TeachersService(), PAGE_TEACHERS.label)
        makeObservable(this, {
            // add: action

        })
    }


    getTeachers = () => {
        this.isItemsLoading = true;
        (this.service as TeachersService)
        .getTeachers()
        .then((data) => {this.teachers = data})
        .catch(() => showErrorMessage())
        .finally(() => {
            this.isItemsLoading = false;
        })
    }

    createTeacher = (data: Partial<ITeacher>) => {
        this.isUpdateLoading = true;
        (this.service as TeachersService)
          .createTeacher(data)
          .then(() => {
            showSuccessMessage()
            this.load()
          })
          .catch(() => {
            showErrorMessage()
          })
          .finally(() => {
            this.isUpdateLoading = false
          })
      }
}

export default TeachersStore