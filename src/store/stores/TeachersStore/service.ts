import { AgentClass } from "@/store/classes/AgentClass";
import { ITeacher } from "./types";
import baseService from "@/store/baseService";

export class TeachersService extends AgentClass<ITeacher>{
    constructor(){
        super('api/teacher', ITeacher)
    }

    getTeachers = async() => {
        const res = await baseService.get('/api/teacher')
        console.log(res.data, 'teachers')
        return res.data
    }

    createTeacher = async(data: Partial<ITeacher>) => {
        await baseService.post('/api/teacher', data)
        console.log(data, 'registered teacher')
        return data
    }
    
}