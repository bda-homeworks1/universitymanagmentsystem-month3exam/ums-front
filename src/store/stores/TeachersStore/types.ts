
export class ITeacher {
  id!: number
  name!: string
  surname!: string
  username!: string
  password!: string
  email!: string
  subject!: string
  roleId?: number 

}

