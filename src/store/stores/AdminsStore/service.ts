import { AgentClass } from "@/store/classes/AgentClass";
import { IAdmin } from "./types";
import baseService from "@/store/baseService";

export class AdminsService extends AgentClass<IAdmin>{
    constructor(){
        super('api/admin', IAdmin)
    }

    getAdmins = async() => {
        const res = await baseService.get('/api/admin')
        console.log({...res.data}, 'dataaaas nadmin')
        return res.data
    }

    createAdmin = async (data: Partial<IAdmin>) => {
            const res = await baseService.post('/api/admin', data);
            console.log({...res.data}, 'Created admin');    
            return res.data;
    };
    
}