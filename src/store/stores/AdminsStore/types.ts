
export class IAdmin {
    id!: number
    name?: string
    surname?: string
    username?: string
    password?: string  
    email?: string
    roleId?: number 
  }
  
  