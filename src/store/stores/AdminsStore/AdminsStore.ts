import FetchableStore from "@/store/classes/FetchableStore";
import { IAdmin } from "./types";
import { AdminsService } from "./service";
import { PAGE_ADMINS } from "@/constants";
import { action, makeObservable } from "mobx";
import { showErrorMessage, showSuccessMessage } from "@/utils";

class AdminsStore extends FetchableStore<IAdmin>{
    admins: IAdmin[] = []

    constructor(){
        super(new AdminsService(), PAGE_ADMINS.label)
        makeObservable(this, {
            // add: action
            getAdmins: action
        })
    }


    getAdmins = async() => {
        this.isItemsLoading = true;
        (this.service as AdminsService)
        .getAdmins()
        .then(async (data) => {
          this.admins  = await data
          console.log(this.admins, 'admins')
            return data})
        .catch(() => showErrorMessage())
        .finally(() => {
            this.isItemsLoading = false;
        })
    }

    createAdmin = (data: Partial<IAdmin>) => {
        this.isUpdateLoading = true;
        (this.service as AdminsService)
          .createAdmin(data)
          .then(() => {
            showSuccessMessage()
            this.load()
          })
          .catch(() => {
            showErrorMessage()
          })
          .finally(() => {
            this.isUpdateLoading = false
          })
      }
}

export default AdminsStore