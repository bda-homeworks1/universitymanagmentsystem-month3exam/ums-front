import { makeAutoObservable } from 'mobx'
import { LoginRequest, AuthUser, LoginRequestResponse, RoleMappings } from './types'
import { showErrorMessage } from '@/utils'
import AuthService from './AuthService'
import { ACCESS_TOKEN_KEY, FULLNAME_LOCAL_STORAGE, ROLE_LOCAL_STORAGE } from '@/constants'
import {Role} from '@/store/stores/AuthStore';
import { Tree } from '@/store/types'

export class AuthStore {
  user: AuthUser | null = null
  authService: AuthService
  isLoading: boolean = false
  tree: Tree = []
  data!: LoginRequestResponse 


  constructor() {
    makeAutoObservable(this)
    this.authService = new AuthService()
  }

  dispose() {
    // this.user = null  
    window.localStorage.removeItem(ACCESS_TOKEN_KEY)
    // window.location.replace('/login')
  }

  get isAdmin() {
    return this.data.role === RoleMappings[Role.ADMIN]
  }

  get isTeacher() {
    return this.data.role === RoleMappings[Role.TEACHER]
  }

  get isStudent() {
    return this.data.role === RoleMappings[Role.STUDENT]
  }

  async login(request: LoginRequest) {
    this.isLoading = true
    return this.authService
      .login(request)
      .then((data) => {
        this.data = data
        this.user = this.data.user
        console.log(this.user)
        console.log('login')
        localStorage.setItem(ACCESS_TOKEN_KEY, data.accessToken)
        localStorage.setItem(ROLE_LOCAL_STORAGE, data.role)
        localStorage.setItem(FULLNAME_LOCAL_STORAGE, data.name + ' ' + data.surname)
      })
      .catch((e) => {
        showErrorMessage()
        throw e
      })
      .finally(() => {
        this.isLoading = false
      })
  }

 async logout() {
    return this.authService
      .logout()
      .then(() => {
        console.log('logout autthstore')
        this.dispose()
      })
      .catch((_) => {
        showErrorMessage()
      })

  }

  async load() {
    this.isLoading = true
    return this.authService
      .load()
      .then((data) => {
        console.log('Authstore load')
        this.data.user = data
      })
      .catch((e) => {
        throw e
      })
      .finally(() => {
        this.isLoading = false
      })
  }

  get isAuthenticated() {
    console.log('is Authenticated', this.user !== null && !this.isLoading && localStorage.getItem(ACCESS_TOKEN_KEY) !== null )
    return  localStorage.getItem(ACCESS_TOKEN_KEY) !== null
  }


}

export const authStore = new AuthStore()
