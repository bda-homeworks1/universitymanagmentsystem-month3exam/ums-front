import { LoginRequest, LoginRequestResponse, AuthUser } from './types'
import baseService from '@/store/baseService';

export default class AuthService {
  async login(dto: LoginRequest): Promise<LoginRequestResponse> {
    const res = await baseService.post<LoginRequestResponse>('/auth', dto)
    return res.data
  }

  async load(): Promise<AuthUser> {
    const res = await baseService.get<AuthUser>('/admin/profile')
    return res.data
  }

  async logout() {
  return baseService.post('/admin/logout')
  }
}
