export class LoginRequest {
  username!: string
  password!: string 
}

export class LoginRequestResponse {
  accessToken: string = ''
  refreshToken: string = ''
  message: string = ''
  user: AuthUser = new AuthUser
  role!: string 
  name!: string
  surname!: string
}

export enum Role {
  ADMIN,
  TEACHER,
  STUDENT
}

export const RoleMappings: Record<Role, string> = {
  [Role.ADMIN]: 'ADMIN',
  [Role.TEACHER]: 'TEACHER',
  [Role.STUDENT]: 'STUDENT',
}

export class AuthUser {
  username?: string
  password?: string
}
