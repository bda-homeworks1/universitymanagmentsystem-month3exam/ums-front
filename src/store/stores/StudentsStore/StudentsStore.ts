import FetchableStore from "@/store/classes/FetchableStore";
import { IStudent } from "./types";
import { StudentsService } from "./service";
import { PAGE_TEACHERS } from "@/constants";
import { action, makeObservable } from "mobx";
import { showErrorMessage, showSuccessMessage } from "@/utils";

class StudentsStore extends FetchableStore<IStudent>{
    students: IStudent[] = []
    

    constructor(){
        super(new StudentsService(), PAGE_TEACHERS.label)
        makeObservable(this, {
            // add: action

        })
    }


    getStudents = () => {
        this.isItemsLoading = true;
        (this.service as StudentsService)
        .getStudents()
        .then((data) => {this.students = data})
        .catch(() => showErrorMessage())
        .finally(() => {
            this.isItemsLoading = false;
        })
    }

    createStudent = (data: Partial<IStudent>) => {
        this.isUpdateLoading = true;
        (this.service as StudentsService)
        .createStudent(data)
        .then(() => {
            showSuccessMessage()
            this.load()
        })
        .catch(() => {
            showErrorMessage()
        })
        .finally(() => {
            this.isUpdateLoading = false
        })
    }

    // add = (data: IStudent) => {
    //     this.isUpdateLoading = true;
    //     (this.service as StudentsService)
    //       .add(data)
    //       .then(() => {
    //         showSuccessMessage()
    //         this.load()
    //       })
    //       .catch(() => {
    //         showErrorMessage()
    //       })
    //       .finally(() => {
    //         this.isUpdateLoading = false
    //       })
    //   }
}

export default StudentsStore