import { AgentClass } from "@/store/classes/AgentClass";
import { IStudent } from "./types";
import baseService from "@/store/baseService";

export class StudentsService extends AgentClass<IStudent>{
    constructor(){
        super('api/student', IStudent)
    }

    getStudents = async() => {
        const res = await baseService.get('/api/student')
        return res.data
    }

    createStudent = async(data: Partial<IStudent>) => {
        baseService.post('/api/student', data)
        return data
    }
    
}