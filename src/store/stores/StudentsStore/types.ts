
export class IStudent {
    id!: number
    name?: string
    surname?: string
    username?: string
    password?: string
    email?: string
    groupId?: number
    roleId?: number
  }
  
  